all: artigo.pdf
	pdflatex artigo.tex
	bibtex artigo
	pdflatex artigo.tex
	pdflatex artigo.tex
	pdflatex artigo.tex

view: all
	evince artigo.pdf
